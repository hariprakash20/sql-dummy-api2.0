const { users } = require("../models");
const db = require("../models");
const User = db.users;
const Op = db.Sequelize.Op;

exports.create = (req, res) => {
  if (!req.body.firstName || !req.body.lastName) {
    res.status(400).send({
      message: "First name or last name cannot be empty!",
    });
    return;
  }

  const user = {
    firstName: req.body.firstName,
    lastName: req.body.lastName,
  };

  User.create(user)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "some error occured while creating the user",
      });
    });
};

exports.findAll = (req, res) => {
  User.findAll()
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message : err.message || "some error occured during fetching the data."
      });
    });
};

exports.findOne = (req, res) =>{
    const id = req.params.id;

    User.findByPk(id)
    .then((data) =>{
        if(data){
            res.send(data);
        }else{
            res.status(404).send({
                message: "cannot find to user with id " + id,
            })
        }
    })
    .catch((err)=>{
        res.status(500).send({
            message: err.message || "some error occured during fetching the data",
        })
    })
};

exports.update = (req, res) => {
    const id = req.params.id;
  
    User.update(req.body, {
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "User was updated successfully."
          });
        } else {
          res.send({
            message: `Cannot update user with id=${id}. Maybe Tutorial was not found or req.body is empty!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Error updating user with id=" + id
        });
      });
  };
