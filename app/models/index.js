const { DATABASE_URL} = require('../config/config')

const Sequelize = require("sequelize");
const sequelize = new Sequelize(DATABASE_URL);
const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.tutorials = require("./tutorial.model.js")(sequelize, Sequelize);
db.users = require('./user.model')(sequelize,Sequelize);

module.exports = db;